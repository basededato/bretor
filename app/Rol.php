<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id_rol'
    ];
    
    public function usuarios()
    {
        return $this->hasMany('Usuario','rolid_rol');
    }
}
