<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $primaryKey = 'id_usuario';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre','apellido','telefono','cedula','direccion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id_usuario','estado','clave','user_created','user_updated','fecha_creado','fecha_ediccion','rolid_rol',
    ];
    
    public function rol()
    {
        return $this->belongsTo('Rol','rolid_rol');
    }
    public function usuario_libros()
    {
        return $this->hasMany('usuario_libro','usuarioid_usuario');
    }
}
