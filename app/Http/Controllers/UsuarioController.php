<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Illuminate\Support\Facades\Redirect;
use DB;
use App\Http\Requests;
use App\Usuario_libro;
use Session;
use Response;

class UsuarioController extends Controller
{
    public function showLogin(){
        if(Session::has('usuario')){
            return view('index');
        }else{
            return view('login');
        }
    }
    
    public function login(Request $request){
        $this->validate($request,[
            'cedula'=> 'required|numeric',
            'clave' => 'required',
            ]);
        $usuario = DB::select('call iniciar_sesion(?,?)', array($request->cedula, $request->clave));
        
        if($usuario == null){
            return Redirect('/')->with('msjError','El usuario no existe en nuestro sistema');
        }else{
            Session::put('usuario',$usuario[0]);
            return Redirect('/');
        }
    }
    
    public function showRegistro(){
        if(Session::has('usuario')){
            return Redirect('/index');
        }else{
            return view('registro');
        }
    }
    
    public function registro(Request $request){
        $this->validate($request,[
            'nombre'=> 'required|max:255',
            'apellido'=> 'required|max:255',
            'clave' => 'required|min:6',
            'cedula'=> 'required|numeric|unique:usuarios',
            'telefono' => 'required',
            'direccion' => 'required|max:100',
        ]);
            
        $respuesta = DB::select('call insertar_usuario(?,?,?,?,?,?,?,?,?)', array($request->nombre,$request->apellido,$request->telefono,
        $request->cedula,$request->direccion,$request->clave,$request->nombre.' '.$request->apellido,$request->nombre.' '.$request->apellido,2));
        
        if($respuesta == null){
            return Redirect('/')->with('msj','El usuario ha sido registrado exitosamente, puede iniciar sesión');
        }else{
            return Redirect('/registro')->with('msjError','No se pudo registrar el usuario, intentelo de nuevo');
        }
    }
    
    public function salir(){
        if(Session::has('usuario')){
            Session::forget('usuario');
        }
        return Redirect('/');;
    }
    
    public function index(){
        return view('index');
    }
    
    public function listado(){
    $usuarios = Usuario::all();
    return view('usuarios')->with('usuarios',$usuarios);
    }
    
    public function showConfig(){
        $user = Usuario::find(Session::get('usuario')->id_usuario);
        return view('configuracions')->with('user',$user);
    }
    
    public function config(Request $request){
        $this->validate($request,[
            'clave' => 'required|min:6',
            'telefono' => 'required',
            'direccion' => 'required|max:100',
        ]);
        
        $respuesta = DB::select('call actualizar_usuario(?,?,?,?)', array($request->telefono,
            $request->direccion,$request->clave,Session::get('usuario')->id_usuario));
        
        
        return Redirect::back()->with('msj','El usuario ha sido registrado exitosamente, puede iniciar sesión');
    }
    
    public function misPedidos(){
        $pedidos = Usuario_libro::where('usuarioid_usuario',Session::get('usuario')->id_usuario)->get();
        return view('mis_pedidos')->with('pedidos',$pedidos);
    }
    
    public function cancerlarPrestamo(Request $request, $id){
        $prestamo = Usuario_libro::find($id);
        
        if($prestamo == null){
            return Redirect::back()->with('msjError','No existe ese prestamo');
        }
        
        $respuesta = DB::select('call editar_prestamo(?,?,?,?)', array($id, 3, $prestamo->libroid_libro,1) );
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Préstamo cancelado');
        }else{
            return Redirect::back()->with('msjError','No se pudo cancelar el préstamo');
        }
    }
    
    public function getEditorial (){
        $json = DB::table('editorial_max')->get();
        return Response::Json($json);
    }
    
    public function getCat (){
        $json = DB::table('categoria_max')->get();
        return Response::Json($json);
    }
    
    public function getAutor (){
        $json = DB::table('autor_max')->get();
        return Response::Json($json);
    }
}
