<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Redirect;

class CategoriaController extends Controller
{
    public function listado(){
        $categorias = Categoria::all();
        return view('categorias')->with('categorias',$categorias);
    }
    
    public function add(Request $request){
        $this->validate($request,[
            'categoria'=> 'required|max:255',
        ]);
        
        $respuesta = DB::select('call insertar_categoria(?)', array($request->categoria));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro agregado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo insertar el registro, inténtelo de nuevo');
        }
        
    }
    
    public function edit(Request $request, $id){
        $this->validate($request,[
            'categoria'=> 'required|max:255',
        ]);
        
        $categoria = Categoria::find($id);
        
        if($categoria==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        $respuesta = DB::select('call actualizar_categoria(?,?)', array($request->categoria,$id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro editado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo editar el registro, inténtelo de nuevo');
        }
        
    }
    
    public function remove($id){
        $categoria = Categoria::find($id);
        
        if($categoria==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        $respuesta = DB::select('call eliminar_categoria(?)', array($id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro eliminado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo eliminar el registro, inténtelo de nuevo');
        }
        
    }
}
