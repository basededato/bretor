<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Autor;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Redirect;

class AutorController extends Controller
{
    public function listado(){
        $autors = Autor::all();
        return view('autors')->with('autors',$autors);
    }
    
    public function add(Request $request){
        $this->validate($request,[
            'autor'=> 'required|max:255',
        ]);
        
        $respuesta = DB::select('call insertar_autor(?)', array($request->autor));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro agregado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo insertar el registro, inténtelo de nuevo');
        }
        
    }
    
    public function edit(Request $request, $id){
        $this->validate($request,[
            'autor'=> 'required|max:255',
        ]);
        
        $autor = Autor::find($id);
        
        if($autor==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        $respuesta = DB::select('call actualizar_autor(?,?)', array($request->autor,$id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro editado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo editar el registro, inténtelo de nuevo');
        }
        
    }

    public function remove($id){
        $autor = Autor::find($id);
        
        if($autor==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        $respuesta = DB::select('call eliminar_autor(?)', array($id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro eliminado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo eliminar el registro, inténtelo de nuevo');
        }
        
    }
    
    
}
