<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editorial;
use App\Http\Requests;
use DB;
use Illuminate\Support\Facades\Redirect;

class EditorialController extends Controller
{
    public function listado(){
        $editorials = Editorial::all();
        return view('editorials')->with('editorials',$editorials);
    }
    
    public function add(Request $request){
    $this->validate($request,[
        'editorial'=> 'required|max:255',
    ]);
    
    $respuesta = DB::select('call insertar_editorial(?)', array($request->editorial));
    
    if($respuesta == null){
        return Redirect::back()->with('msj','Registro agregado correctamente');
    }else{
        return Redirect::back()->with('msjError','No se pudo insertar el registro, inténtelo de nuevo');
    }
    
}

    public function edit(Request $request, $id){
        $this->validate($request,[
            'editorial'=> 'required|max:255',
        ]);
        
        $editorial = Editorial::find($id);
        
        if($editorial==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        $respuesta = DB::select('call actualizar_editorial(?,?)', array($request->editorial,$id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro editado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo editar el registro, inténtelo de nuevo');
        }
        
    }

    public function remove($id){
        $editorial = Editorial::find($id);
        
        if($editorial==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        $respuesta = DB::select('call eliminar_editorial(?)', array($id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro eliminado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo eliminar el registro, inténtelo de nuevo');
        }
        
    }
}
