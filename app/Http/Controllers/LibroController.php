<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libro;
use App\Categoria;
use App\Autor;
use App\Editorial;
use App\Usuario_libro;
use DB;
use Illuminate\Support\Facades\Redirect;
use Storage;
use Session;
use Carbon\Carbon;

class LibroController extends Controller
{
    public function listado(){
        if(Session::has('usuario')){
            $libros = Libro::all();
            
            if(Session::get('usuario')->rolid_rol==1){
                return view('libros')->with('libros',$libros);                  
            }
            
            return view('libro_users')->with('libros',$libros);  
  
        }
        
        
    }
    
    
    public function crear(){
        $categorias = Categoria::all();
        $autores = Autor::all();
        $editoriales = Editorial::all();
        return view('crear_libros')->with(['categorias'=>$categorias, 'editoriales'=>$editoriales, 'autores'=>$autores]);
    }
    
    public function add(Request $request){
        $this->validate($request,[
            'isbn'=> 'required|max:55',
            'titulo' => 'required|max:50',
            'portada' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
            'descripcion' => 'required|max:2000',
            'fecha' => 'required|date_format:Y-m-d',
            'categoria' =>'required',
            'editorial' => 'required',
            'autor' => 'required',
        ]);
        
        $img = $request->file('portada');
        $file_route = 'Libro'.time().'.jpg';
        
        Storage::disk('imgLibros')->put($file_route, file_get_contents ( $img->getRealPath() ) );
        
        $respuesta = DB::select('call insertar_libro(?,?,?,?,?,?,?,?)', array($request->isbn, $request->titulo, $file_route, 
                    $request->descripcion,$request->fecha,$request->categoria, $request->editorial, $request->autor));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro agregado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo insertar el registro, inténtelo de nuevo');
        }
        
    }

    public function remove($id){
        $libro = Libro::find($id);
        
        if($libro==null){
            return Redirect::back()->with('msjError','No existe ese registro');
        }
        
        Storage::disk('imgLibros')->delete($libro->portada);
        
        $respuesta = DB::select('call eliminar_libro(?)', array($id));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Registro eliminado correctamente');
        }else{
            return Redirect::back()->with('msjError','No se pudo eliminar el registro, inténtelo de nuevo');
        }
        
    }
    
    public function rentar(Request $request, $id){
        $libro = Libro::find($id);
        
        if($libro == null){
            return Redirect::back()->with('msjError','No existe el libro');
        }
        
        $respuesta = DB::select('call prestar_libro(?,?,?)', array(Session::get('usuario')->id_usuario, $id, Carbon::now()->addDay(7) ));
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Libro rentado, puedes mirar en el listado de tus prestamos');
        }else{
            return Redirect::back()->with('msjError','No se pudo rentar el libro');
        }
        
    }
    
    public function prestamos(){
        $pedidos = Usuario_libro::all();
        return view('prestamos')->with('pedidos',$pedidos);
    }
    
    public function editarPrestamo(Request $request, $id, $opcion){
        $prestamo = Usuario_libro::find($id);
        
        if($prestamo == null){
            return Redirect::back()->with('msjError','No existe ese prestamo');
        }
        if($opcion == 5){
            $respuesta = DB::select('call editar_prestamo(?,?,?,?)', array($id, 5, $prestamo->libroid_libro,1) );
        }else{
            $respuesta = DB::select('call editar_prestamo(?,?,?,?)', array($id, 4, $prestamo->libroid_libro,0) );
        }
        
        if($respuesta == null){
            return Redirect::back()->with('msj','Préstamo editado');
        }else{
            return Redirect::back()->with('msjError','No se pudo editar el préstamo');
        }
    }
    
    
}
