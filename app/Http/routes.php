<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UsuarioController@showLogin');
Route::get('/registro', 'UsuarioController@showRegistro');
Route::post('/registro','UsuarioController@registro');
Route::get('/index', 'UsuarioController@index');
Route::post('/login', 'UsuarioController@login');
Route::any('/salir', 'UsuarioController@salir');

Route::get('/categorias','CategoriaController@listado');
Route::post('/addCategoria','CategoriaController@add');
Route::any('/editCategoria/{id}','CategoriaController@edit');
Route::delete('/removeCategoria/{id}','CategoriaController@remove');

Route::get('/editorials','EditorialController@listado');
Route::post('/addEditorial','EditorialController@add');
Route::any('/editEditorial/{id}','EditorialController@edit');
Route::delete('/removeEditorial/{id}','EditorialController@remove');

Route::get('/autors','AutorController@listado');
Route::post('/addAutor','AutorController@add');
Route::any('/editAutor/{id}','AutorController@edit');
Route::delete('/removeAutor/{id}','AutorController@remove');

Route::get('/libros','LibroController@listado');
Route::get('/crearLibro','LibroController@crear');
Route::post('/addLibro','LibroController@add');
Route::delete('/removeLibro/{id}','LibroController@remove');
Route::post('/editarPrestamo/{id}/{opcion}','LibroController@editarPrestamo');
Route::get('/prestamos','LibroController@prestamos');

Route::get('/usuarios','UsuarioController@listado');
Route::get('/misPrestamos','UsuarioController@misPedidos');

Route::get('/configuracions', 'UsuarioController@showConfig');
Route::post('/config', 'UsuarioController@config');
Route::post('/rentar/{id}','LibroController@rentar');
Route::post('/cancelarPrestamo/{id}','UsuarioController@cancerlarPrestamo');

Route::get('/librosEditorial', 'UsuarioController@getEditorial');
Route::get('/librosAutor', 'UsuarioController@getAutor');
Route::get('/librosCategoria', 'UsuarioController@getCat');