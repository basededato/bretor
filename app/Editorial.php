<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
     protected $primaryKey = 'id_editorial';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'editorial'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id_editorial'
    ];
    public function libros()
    {
        return $this->hasMany(Libro::class,'editorialid_editorial');
    }
}
