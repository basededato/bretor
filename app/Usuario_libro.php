<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario_libro extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id','usuarioid_usuario','libroid_libro','fecha_renta','fecha_devolucion','estadoid_estado'
    ];
    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuarioid_usuario');
    }
    public function libro()
    {
        return $this->belongsTo(Libro::class,'libroid_libro');
    }
    public function estado()
    {
        return $this->belongsTo(Estado::class,'estadoid_estado');
    }
}
