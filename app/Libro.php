<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    
    protected $primaryKey = 'id_libro';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isbn','titulo','portada','descripcion','fecha_publicacion','autor_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id_libro','estado','categoriaid_categoria','editorialid_editorial'
    ];
    public function categoria()
    {
        return $this->belongsTo(Categoria::class,'categoriaid_categoria');
    }
    public function editorial()
    {
        return $this->belongsTo(Editorial::class,'editorialid_editorial');
    }
    public function autor()
    {
        return $this->belongsTo(Autor::class,'autor_id');
    }
}
