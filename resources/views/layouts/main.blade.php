<!DOCTYPE html>
<html lang="en">
<head>
    <title>Biblioteca</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="css/uniform.css" />
    <link rel="stylesheet" href="css/select2.css" />
    <link rel="stylesheet" href="css/matrix-style.css" />
    <link rel="stylesheet" href="css/matrix-media.css" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/jquery.gritter.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
  
    @if (session()->has('usuario'))
    <div id="header">
      <h1><a href="dashboard.html">Biblioteca</a></h1>
    </div>
    <div id="user-nav" class="navbar navbar-inverse">
      <ul class="nav">
        <li class=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Bienvenido {!!session('usuario')->nombre!!}</span></a></li>
        <li class=""><a title="" href="/salir"><i class="icon icon-share-alt"></i> <span class="text">Salir</span></a></li>
      </ul>
    </div>

    <div id="sidebar"><a href="/index" class="visible-phone"><i class="icon icon-home"></i> Inicio</a>
      <ul>
        <li><a href="/index"><i class="icon icon-home"></i> <span>Inicio</span></a> </li>
        @if(session('usuario')->rolid_rol==1)
          <li><a href="/categorias"><i class="icon icon-list-ul"></i> <span>Categorías</span></a> </li>
          <li><a href="/autors"><i class="icon icon-group"></i> <span>Autores</span></a> </li>
          <li><a href="/editorials"><i class="icon icon-bookmark-empty"></i> <span>Editorial</span></a> </li>
          <li><a href="/libros"><i class="icon icon-book"></i> <span>Libros</span></a></li>
          <li><a href="/prestamos"><i class="icon-check"></i> <span>Préstamos</span></a></li>
          <li><a href="/usuarios"><i class="icon icon-user"></i> <span>Usuarios</span></a></li>
        @else
          <li><a href="/libros"><i class="icon icon-book"></i> <span>Libros</span></a></li>
          <li><a href="/misPrestamos"><i class=" icon-shopping-cart"></i> <span>Mis prestamos</span></a></li>
        @endif
        <li><a href="/configuracions"><i class="icon icon-cog"></i> <span>Configuración</span></a></li>
      </ul>
    </div>

    <div id="content">
      <div class="container-fluid">
          @include('errors.error')
          @yield('contenido')
      </div>
    </div>


    <div class="row-fluid">
      <div id="footer" class="span12"> 2013 &copy; Matrix Admin. Brought to you by <a href="http://themedesigner.in">Themedesigner.in</a> </div>
    </div>
    @else
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        Permiso no válido para ingresar a esta página
      </div>
    @endif
    
    
    
    <script src="js/jquery.min.js"></script> 
    <script src="js/bootstrap.min.js"></script> 
    @yield('scripts')
    <script src="js/jquery.ui.custom.js"></script> 
    <script src="js/bootstrap-datepicker.js"></script> 
    <script src="js/jquery.uniform.js"></script> 
    <script src="js/jquery.dataTables.min.js"></script> 
    <script src="js/matrix.js"></script> 
    <script src="js/matrix.tables.js"></script>
    <script src="js/select2.min.js"></script> 
    
</body>
</html>
