@extends('layouts.main')

@section('contenido')

<h1>Mis préstamos</h1>

<div>
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Mis préstamos</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th colspan="2" width="35%">Libro</th>
              <th width="10%">Editorial</th>
              <th width="15%">Autor</th>
              <th width="10%">Categoría</th>
              <th width="10%">Fecha prestamo</th>
              <th width="10%">Fecha devolución</th>
              <th width="15%">Opciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($pedidos as $c)
                <tr class="gradeA">
                  <td width="10%"> <img src="/imgLibros/{{$c->libro->portada}}" width="100"></img></td>
                  <td width="25%">
                    <h6>{!!$c->libro->titulo!!}</h6>
                    <p>{!!$c->libro->descripcion!!}</p>
                  </td>
                  <td width="10%">{{$c->libro->editorial->editorial}}</td>
                  <td width="15%">{{$c->libro->autor->nombre}}</td>
                  <td width="10%">{{$c->libro->categoria->categoria}}</td>
                  <td width="10%">{{ Carbon\Carbon::parse($c->fecha_renta)->format('d/m/Y') }}</td>
                  <td width="10%">{{ Carbon\Carbon::parse($c->fecha_devolucion)->format('d/m/Y') }}</td>
                  <td style="text-align: center;" width="15%">
                    @if($c->estado->estado=="rentado")
                      <form role="form" method="POST" action="{{ url('/editarPrestamo/'.$c->id.'/4') }}">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-primary btn-small">Entregar libro</button>
                      </form>
                    @elseif($c->estado->estado=="entregado")
                      <form role="form" method="POST" action="{{ url('/editarPrestamo/'.$c->id.'/5') }}">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-success btn-small">Libro devuelto</button>
                      </form>
                    @else
                        <span>{{$c->estado->estado}}</span>
                    @endif
                  </td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection