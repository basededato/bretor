@extends('layouts.main')

@section('contenido')
<h1>Configuracion</h1>

<div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Editar datos</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{url('/config')}}" method="POST" class="form-horizontal">
            {!! csrf_field() !!}
            
            <div class="control-group">
              <label class="control-label">Teléfono</label>
              <div class="controls">
                <input type="text" class="span11" name="telefono" value="{{$user->telefono}}"  placeholder="Ingrese el telefono" />
                @if ($errors->has('telefono'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('telefono') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Direccion</label>
              <div class="controls">
                <input type="text" name="direccion" class="span11" value="{{$user->direccion}}"  placeholder="Ingrese la direccion" />
                @if ($errors->has('direccion'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('direccion') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Contraseña</label>
              <div class="controls">
                <input type="text" name="clave" class="span11" placeholder="Ingrese la nueva contraseña" />
                @if ($errors->has('clave'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('clave') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            
            <div class="form-actions">
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection