@extends('layouts.main')

@section('contenido')

<h1>Listado de libros</h1>

<div style="padding: 1em 0em;">
  <a class="btn btn-success" href="/crearLibro"><i class="icon-plus-sign"></i> Agregar</a>  
</div>

<div>
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Libros</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th colspan="2" width="35%">Libro</th>
              <th width="10%">ISBN</th>
              <th width="10%">Editorial</th>
              <th width="15%">Autor</th>
              <th width="10%">Categoría</th>
              <th width="10%">Publicación</th>
              <th width="15%">Opciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($libros as $c)
                <tr class="gradeA">
                  <td width="10%"> <img src="/imgLibros/{{$c->portada}}" width="100"></img></td>
                  <td width="25%">
                    <h6>{!!$c->titulo!!}</h6>
                    <p>{!!$c->descripcion!!}</p>
                  </td>
                  <td width="10%">{{$c->isbn}}</td>
                  <td width="10%">{{$c->editorial->editorial}}</td>
                  <td width="15%">{{$c->autor->nombre}}</td>
                  <td width="10%">{{$c->categoria->categoria}}</td>
                  <td width="10%">{{ Carbon\Carbon::parse($c->fecha_publicacion)->format('d/m/Y') }}</td>
                  <td style="text-align: center;" width="15%">
                      <button class="elim_libro btn btn-danger" title="Eliminar" data-target="#delete" data-toggle="modal" data-id="{{ $c->id_libro }}"><i class="icon-remove" aria-hidden="true"> </i></button>
                  </td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
</div>

<div id="delete" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Eliminar autor</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" id="form_elim">
      <div class="modal-body">
        {!! csrf_field() !!}
        <input type="hidden" value="DELETE" name="_method"/>
        <div class="form-group">
            <p>¿Está seguro de eliminar este libro?</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Si</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
  </form>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).on("click", ".elim_libro", function () {
            var Id = $(this).data('id');

            var formAction ="/removeLibro/"+Id
            $('#form_elim').attr('action', formAction);
        });
    </script>
@endsection