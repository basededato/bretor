<!DOCTYPE html>
<html>
    <head>
        <title>Biblioteca</title>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="Alex Brequeman"/>
        <link rel="stylesheet" href="/bootstrap-3.3.7-dist/css/bootstrap.min.css" type="text/css" />
    </head>
    <body>
        <div class="container">
            <div class="page-header">
              <h1 class="text-center">Sistema Bibliotecario</h1>
            </div>
            <div class="row" style="margin-top:1em;">
                <div class="col-md-offset-4 col-md-4">
                    <div class="panel panel-default">
                        @include('errors.error')
                      <div class="panel-heading">Registrarse</div>
                      <div class="panel-body">
                        <form method="POST" action="{{ url('/registro') }}">
                            {!! csrf_field() !!}
                           <div class="form-group">
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                            @if ($errors->has('nombre'))
	                        <span class="text-danger">
	                            <strong>{{ $errors->first('nombre') }}</strong>
	                        </span>
	                    	@endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido">
                            @if ($errors->has('apellido'))
	                        <span class="text-danger">
	                            <strong>{{ $errors->first('apellido') }}</strong>
	                        </span>
	                    	@endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" id="cedula" name="cedula" placeholder="Cedula">
                                @if ($errors->has('cedula'))
    	                        <span class="text-danger">
    	                            <strong>{{ $errors->first('cedula') }}</strong>
    	                        </span>
    	                    	@endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono">
                               @if ($errors->has('telefono'))
    	                        <span class="text-danger">
    	                            <strong>{{ $errors->first('telefono') }}</strong>
    	                        </span>
    	                    	@endif
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direccion">
                               @if ($errors->has('direccion'))
    	                        <span class="text-danger">
    	                            <strong>{{ $errors->first('direccion') }}</strong>
    	                        </span>
    	                    	@endif
                          </div>
                         <div class="form-group">
                            <input type="password" class="form-control" id="clave" name="clave" placeholder="Contraseña">
                               @if ($errors->has('clave'))
    	                        <span class="text-danger">
    	                            <strong>{{ $errors->first('clave') }}</strong>
    	                        </span>
    	                    	@endif
                          </div>
                          <button type="submit"  class="btn btn-primary btn-lg btn-block">Registrar</button>
                          <a href="/" class="pull-right">Iniciar sesión</a>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <script type="text/javascript" src="/jquery-3.2.0.min"></script>
        <script type="text/javascript" src="/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    </body>
</html>