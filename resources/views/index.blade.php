@extends('layouts.main')

@section('contenido')

<h1>Hola {!!session('usuario')->nombre!!}, bienvenido al sistema bibliotecario</h1>


<div class="widget-box" style="margin-top:2em;">
        @if(session('usuario')->rolid_rol==1)
            <div class="row-fluid">
              <div class="span6">
                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
                    <h5>Libros por editoriales</h5>
                  </div>
                  <div class="widget-content">
                    <div id="container"></div>
                  </div>
                </div>
              </div>
              <div class="span6">
                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
                    <h5>Libros por categorías</h5>
                  </div>
                  <div class="widget-content">
                    <div id="container2"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row-fluid">
              <div class="span12">
                <div class="widget-box">
                  <div class="widget-title"> <span class="icon"> <i class="icon-signal"></i> </span>
                    <h5>Libros por autores</h5>
                  </div>
                  <div class="widget-content">
                    <div id="container3"></div>
                  </div>
                </div>
              </div>
            </div>
        @else
        <div class="widget-title">
            <span class="icon"> <i class=" icon-flag"></i> </span>
            <h5>Bienvenido</h5>
        </div>
        <div class="widget-content">
            <p>Aquí podrás ver el listado de los libros que ofrecemos en nuestra biblioteca, ver tus pedidos, solicitar el préstamo de un libro para luego reclamarlo de forma presencial en nuestra biblioteca.</p>
            <p>Te invitamos a navegar entre las opciones que te ofrecemos.</p>
        </div>
        @endif
</div>
    



@endsection

@section('scripts')

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <script type="text/javascript">
        $.get('/librosEditorial', function(info){
    
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Libros por editorial'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Libros',
                    colorByPoint: true,
                    data: info
                }]
            });
        });
        
        $.get('/librosCategoria', function(info){
    
            Highcharts.chart('container2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Libros por categorías'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Libros',
                    colorByPoint: true,
                    data: info
                }]
            });
        });
        
        $.get('/librosAutor', function(info){
    
            Highcharts.chart('container3', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Libros por autores'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Libros',
                    colorByPoint: true,
                    data: info
                }]
            });
        });
    </script>

@endsection