@extends('layouts.main')

@section('contenido')

<h1>Listado de editoriales</h1>

<div style="padding: 1em 5em;">
  <button class="btn btn-success" data-target="#add" data-toggle="modal"><i class="icon-plus-sign"></i> Agregar</button>  
</div>


<div style="padding: 0em 5em;">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Editoriales</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th width="80%">Editorial</th>
              <th width="20%">Opciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($editorials as $c)
                <tr class="gradeA">
                  <td width="80%">{!!$c->editorial!!}</td>
                  <td width="20%" style="text-align: center;">
                      <button class="edit_editorial btn btn-primary" title="Editar" data-target="#edit" data-toggle="modal" data-id="{{ $c->id_editorial }}" data-ed="{!!$c->editorial!!}"><i class="icon-edit" aria-hidden="true"> </i></button>
                      <button class="elim_editorial btn btn-danger" title="Eliminar" data-target="#delete" data-toggle="modal" data-id="{{ $c->id_editorial }}"><i class="icon-remove" aria-hidden="true"> </i></button>
                  </td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
</div>

<div id="add" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Crear Editorial</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" action="{{ url('/addEditorial') }}">
      <div class="modal-body">
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="control-label" for="editorial"><span style="color:red;">* </span>Editorial</label>
            <div class="controls">
                <input type="text" class="form-control" name="editorial" placeholder="Ingrese la editorial" required="El nombre de la editorial es requerido">
                @if ($errors->has('edit_editorial'))
                <span class="help-block">
                    <strong>{{ $errors->first('editorial') }}</strong>
                </span>
                @endif
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>

<div id="edit" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Editar Editorial</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" id="form_edit">
      <div class="modal-body">
        {!! csrf_field() !!}
        <input type="hidden" value="PUT" name="_method"/>
        <div class="form-group">
            <label class="control-label" for="editorial"><span style="color:red;">* </span>Editorial</label>
            <div class="controls">
                <input type="text" class="form-control" name="editorial" id="ed_edit" required="El nombre de la editorial es requerido">
                @if ($errors->has('editorial'))
                <span class="help-block">
                    <strong>{{ $errors->first('editorial') }}</strong>
                </span>
                @endif
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>

<div id="delete" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Eliminar editorial</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" id="form_elim">
      <div class="modal-body">
        {!! csrf_field() !!}
        <input type="hidden" value="DELETE" name="_method"/>
        <div class="form-group">
            <p>¿Está seguro de eliminar esta editorial?</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Si</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
  </form>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).on("click", ".edit_editorial", function () {
            var Id = $(this).data('id');
            var ed = $(this).data('ed');
            $("#ed_edit").val(ed)

            var formAction ="/editEditorial/"+Id
            $('#form_edit').attr('action', formAction);
        });
        $(document).on("click", ".elim_editorial", function () {
            var Id = $(this).data('id');

            var formAction ="/removeEditorial/"+Id
            $('#form_elim').attr('action', formAction);
        });
    </script>
@endsection