@extends('layouts.main')

@section('contenido')

<h1>Listado de usuarios</h1>


<div style="padding: 0em 1em;">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Usuarios</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th width="20%">Nombre</th>
              <th width="20%">Apellido</th>
              <th width="20%">Telefono</th>
              <th width="15%">Cedula</th>
              <th width="25%">Direccion</th>
            </tr>
          </thead>
          <tbody>
            @foreach($usuarios as $c)
                <tr class="gradeA">
                  <td width="20%">{!!$c->nombre!!}</td>
                  <td width="20%">{{$c->apellido}}</td>
                  <td width="20%">{{$c->telefono}}</td>
                  <td width="15%">{{$c->cedula}}</td>
                  <td width="25%">{{$c->direccion}}</td>
                </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection