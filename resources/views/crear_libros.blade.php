@extends('layouts.main')

@section('contenido')
<h1>Crear libro</h1>

<div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Crear libro</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="{{url('/addLibro')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="control-group">
              <label class="control-label">Título</label>
              <div class="controls">
                <input type="text" class="span11" name="titulo" placeholder="Ingrese el título del libro" />
                @if ($errors->has('titulo'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('titulo') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">ISBN</label>
              <div class="controls">
                <input type="text" name="isbn" class="span11" placeholder="Ingrese el isbn" />
                @if ($errors->has('isbn'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('isbn') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Descripción</label>
              <div class="controls">
                <textarea class="span11" name="descripcion" placeholder="Escribe una descripción breve del libro"></textarea>
                @if ($errors->has('descripcion'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('descripcion') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Categoría</label>
              <div class="controls">
                <select name="categoria">
                    @foreach($categorias as $c)
                      <option value="{{$c->id_categoria}}">{{$c->categoria}}</option>
                    @endforeach
                </select>
                @if ($errors->has('categoria'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('categoria') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Editorial</label>
              <div class="controls">
                <select name="editorial">
                    @foreach($editoriales as $e)
                      <option value="{{$e->id_editorial}}">{{$e->editorial}}</option>
                    @endforeach
                </select>
                @if ($errors->has('editorial'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('editorial') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Autor</label>
              <div class="controls">
                <select name="autor">
                    @foreach($autores as $a)
                      <option value="{{$a->id_autor}}">{{$a->nombre}}</option>
                    @endforeach
                </select>
                @if ($errors->has('autor'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('autor') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Fecha publicación</label>
              <div class="controls">
                <input type="date" class="datepicker span3" name="fecha">
                @if ($errors->has('fecha'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('fecha') }}</strong>
                </span>
            	@endif
            </div>
            
            <div class="control-group">
              <label class="control-label">Portada</label>
              <div class="controls">
                <input type="file" name="portada" accept="image/gif, image/jpeg, image/png"/>
                @if ($errors->has('portada'))
                <br/>
                <span class="text-danger">
                    <strong>{{ $errors->first('portada') }}</strong>
                </span>
            	@endif
              </div>
            </div>
            <div class="form-actions">
                <a href="/libros" class="btn btn-danger">Cerrar</a>
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>


@endsection