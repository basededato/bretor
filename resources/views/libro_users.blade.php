@extends('layouts.main')

@section('contenido')

<h1>Listado de libros</h1>

<div>
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Libros</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th colspan="2" width="35%">Libro</th>
              <th width="10%">ISBN</th>
              <th width="10%">Editorial</th>
              <th width="15%">Autor</th>
              <th width="10%">Categoría</th>
              <th width="10%">Publicación</th>
              <th width="15%">Opciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($libros as $c)
                <tr class="gradeA">
                  <td width="10%"> <img src="/imgLibros/{{$c->portada}}" width="100"></img></td>
                  <td width="25%">
                    <h6>{!!$c->titulo!!}</h6>
                    <p>{!!$c->descripcion!!}</p>
                  </td>
                  <td width="10%">{{$c->isbn}}</td>
                  <td width="10%">{{$c->editorial->editorial}}</td>
                  <td width="15%">{{$c->autor->nombre}}</td>
                  <td width="10%">{{$c->categoria->categoria}}</td>
                  <td width="10%">{{ Carbon\Carbon::parse($c->fecha_publicacion)->format('d/m/Y') }}</td>
                  <td style="text-align: center;" width="15%">
                    @if($c->estado)
                      <form role="form" method="POST" action="{{ url('/rentar/'.$c->id_libro) }}">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-primary">Rentar</button>
                      </form>
                    @else
                      <span style="color:red;">No disponible</span>
                    @endif
                  </td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
</div>
@endsection