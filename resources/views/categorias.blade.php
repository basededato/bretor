@extends('layouts.main')

@section('contenido')

<h1>Listado de categorías</h1>

<div style="padding: 1em 5em;">
  <button class="btn btn-success" data-target="#add" data-toggle="modal"><i class="icon-plus-sign"></i> Agregar</button>  
</div>


<div style="padding: 0em 5em;">
    <div class="widget-box">
      <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Categorías</h5>
      </div>
      <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th width="80%">Categoria</th>
              <th width="20%">Opciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($categorias as $c)
                <tr class="gradeA">
                  <td width="80%">{!!$c->categoria!!}</td>
                  <td width="20%" style="text-align: center;">
                      <button class="edit_categoria btn btn-primary" title="Editar" data-target="#edit" data-toggle="modal" data-id="{{ $c->id_categoria }}" data-cat="{!!$c->categoria!!}"><i class="icon-edit" aria-hidden="true"> </i></button>
                      <button class="elim_categoria btn btn-danger" title="Eliminar" data-target="#delete" data-toggle="modal" data-id="{{ $c->id_categoria }}"><i class="icon-remove" aria-hidden="true"> </i></button>
                  </td>
                </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
</div>

<div id="add" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Crear categoría</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" action="{{ url('/addCategoria') }}">
      <div class="modal-body">
        {!! csrf_field() !!}
        <div class="form-group">
            <label class="control-label" for="categoria"><span style="color:red;">* </span>Categoría</label>
            <div class="controls">
                <input type="text" class="form-control" name="categoria" placeholder="Ingrese la categoría" required="El nombre de la categoría es requerido">
                @if ($errors->has('categoria'))
                <span class="help-block">
                    <strong>{{ $errors->first('categoria') }}</strong>
                </span>
                @endif
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>

<div id="edit" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Editar categoría</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" id="form_edit">
      <div class="modal-body">
        {!! csrf_field() !!}
        <input type="hidden" value="PUT" name="_method"/>
        <div class="form-group">
            <label class="control-label" for="categoria"><span style="color:red;">* </span>Categoría</label>
            <div class="controls">
                <input type="text" class="form-control" name="categoria" id="cat_edit" required="El nombre de la categoría es requerido">
                @if ($errors->has('categoria'))
                <span class="help-block">
                    <strong>{{ $errors->first('categoria') }}</strong>
                </span>
                @endif
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
  </form>
</div>

<div id="delete" class="modal fade" role="dialog">
  <div class="modal-header">
    <button data-dismiss="modal" class="close" type="button">×</button>
    <h3>Eliminar categoría</h3>
  </div>
  
  <form class="form-horizontal" role="form" method="POST" id="form_elim">
      <div class="modal-body">
        {!! csrf_field() !!}
        <input type="hidden" value="DELETE" name="_method"/>
        <div class="form-group">
            <p>¿Está seguro de eliminar esta categoría?</p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Si</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
  </form>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).on("click", ".edit_categoria", function () {
            var Id = $(this).data('id');
            var cat = $(this).data('cat');
            $("#cat_edit").val(cat)

            var formAction ="/editCategoria/"+Id
            $('#form_edit').attr('action', formAction);
        });
        $(document).on("click", ".elim_categoria", function () {
            var Id = $(this).data('id');

            var formAction ="/removeCategoria/"+Id
            $('#form_elim').attr('action', formAction);
        });
    </script>
@endsection