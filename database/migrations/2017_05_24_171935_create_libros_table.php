<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libros', function (Blueprint $table) {
            $table->increments('id_libro');
            $table->string('isbn',55);
            $table->string('titulo',50);
            $table->string('portada',255);
            $table->text('descripcion');
            $table->date('fecha_publicacion');
            $table->integer('categoriaid_categoria')->unsigned();
            $table->integer('autor_id')->unsigned();
            $table->smallInteger('estado')->default(1);
            $table->integer('editorialid_editorial')->unsigned();
            $table->foreign('categoriaid_categoria')->references('id_categoria')->on('categorias');
            $table->foreign('editorialid_editorial')->references('id_editorial')->on('editorials');
            $table->foreign('autor_id')->references('id_autor')->on('autors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('libros');
    }
}
