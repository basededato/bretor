<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id_usuario');
            $table->string('nombre',30);
            $table->string('apellido',50);
            $table->string('telefono',10);
            $table->integer('cedula')->unique();
            $table->string('direccion',50);
            $table->smallInteger('estado');
            $table->string('clave',32);
            $table->string('user_created',40);
            $table->string('user_updated',40);
            $table->timestamp('fecha_creado');
            $table->timestamp('fecha_ediccion');
            $table->integer('rolid_rol')->unsigned();
            $table->foreign('rolid_rol')->references('id_rol')->on('rols');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuarios');
    }
}
