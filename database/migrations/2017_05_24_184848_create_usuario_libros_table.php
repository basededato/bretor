<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioLibrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_libros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuarioid_usuario')->unsigned();
            $table->integer('libroid_libro')->unsigned();
            $table->date('fecha_renta');
            $table->date('fecha_devolucion');
            $table->boolean('estadoid_estado')->default(true);
            $table->foreign('usuarioid_usuario')->references('id_usuario')->on('usuarios');
            $table->foreign('libroid_libro')->references('id_libro')->on('libros');
            $table->foreign('estadoid_estado')->references('id_estado')->on('estados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuario_libros');
    }
}
